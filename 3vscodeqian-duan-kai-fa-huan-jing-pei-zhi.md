[https://anran758.github.io/blog/2018/02/06/VSCode-从零开始配置前端环境/](https://anran758.github.io/blog/2018/02/06/VSCode-从零开始配置前端环境/)

快捷键的设置eclipse

```
[  
    { "key": "alt+/",  "command": "editor.action.triggerSuggest","when": "editorTextFocus" },  
    { "key": "ctrl+d", "command": "editor.action.deleteLines","when": "editorTextFocus" },  
    { "key": "ctrl+alt+down","command": "editor.action.copyLinesDownAction", "when": "editorTextFocus" },  
    { "key": "ctrl+alt+up", "command": "editor.action.copyLinesUpAction", "when": "editorTextFocus" },  
    { "key": "shift+enter", "command": "editor.action.insertLineAfter", "when": "editorTextFocus && !editorReadonly" }  ,
    {"key":"ctrl+w","command": "extension.openInBrowser" , "when": "editorTextFocus" }
]

```

配饰sass，less安装插件easy sass,easy less

