### 1.安装cnpm

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

或者设置一下npm的仓库也行

```
npm config set registry https://registry.npm.taobao.org
```

### 2.安装ruby

```
//在命令行中输入ruby -v,有提示表示安装成功
ruby -v
```

### 3.使用gem安装sass

```
gem sources --remove https://rubygems.org/
//替换走taobao的仓库
gem sources -a https://ruby.taobao.org/
gem sources -l
gem install sass
gem install compass

sass -v
//有输出表示安装成功
```

```
//更新sass
gem update sass
```

### 4.在`install package`中安装以下三个插件

* **Sass**

* **Sass Build  **使用 Ctrl + B 快捷键编译

* **SublimeOnSaveBuild  **用于 Ctrl + S 保存文件的时候自动编译

# [sass教程](https://www.jianshu.com/p/0f96f42b6924)



